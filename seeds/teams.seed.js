const mongoose = require('mongoose');
const Team = require('../models/Team.model');
const { DB_URL, DB_CONFIG } = require('../db');
const User = require('../models/User.model');

// name: { type: String, required: true },
// image: { type: String, required: true },
// users: [{ type: mongoose.Types.ObjectId, ref: 'Users' }],
// admin: {type: mongoose.Types.ObjectId, ref: 'Users'},

const TeamsArray = [
    {
        name: 'Real Bañil',
        image: 'http://1000marcas.net/wp-content/uploads/2020/01/Real-Madrid-logo.jpg',
        users: [],
        admin: '',
    },
    {
        name: 'Club de Amigos',
        image: 'https://i.pinimg.com/originals/37/53/91/37539180d5476fb63e5def4424a767c1.png',
        users: [],
        admin: '',
    },
    {
        name: 'Futbol Club',
        image: 'https://upload.wikimedia.org/wikipedia/commons/c/c3/Escudo_del_Club_Atl%C3%A9tico_Boca_Juniors_2007.png',
        users: [],
        admin: '',
    },
    {
        name: 'Barcelona FC',
        image: 'https://i.pinimg.com/originals/37/f7/13/37f713d52fe2fd8d4507c6d232e1d4e6.jpg',
        users: [],
        admin: '',
    },
    {
        name: 'Equipo de Amigos',
        image: 'https://i.pinimg.com/474x/11/77/d4/1177d4059a8e4360594030c1f98a868b.jpg',
        users: [],
        admin: '',
    },
    {
        name: 'Club de barrio',
        image: 'http://as00.epimg.net/img/comunes/fotos/fichas/equipos/large/53.png',
        users: [],
        admin: '',
    },
    {
        name: 'AC Milan',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Logo_of_AC_Milan.svg/1200px-Logo_of_AC_Milan.svg.png',
        users: [],
        admin: '',
    },
    {
        name: 'Club de la Amistad',
        image: 'https://static8.depositphotos.com/1314446/904/v/600/depositphotos_9049166-stock-illustration-football-shield.jpg',
        users: [],
        admin: '',
    },
    {
        name: 'Amigos FC',
        image: 'https://image.freepik.com/vector-gratis/balon-futbol-cinta_24908-61868.jpg',
        users: [],
        admin: '',
    },
    {
        name: 'Los Mejores',
        image: 'https://escudoscr.stg7.net/logo-400.png',
        users: [],
        admin: '',
    },
];

mongoose.connect(DB_URL, DB_CONFIG)
    .then(async () => {
        console.log('Seed de teams ejecutada');

        const allTeams = await Team.find();

        if(allTeams.length) {
            await Team.collection.drop();
            console.log('Colleción de equipos eliminada');
        }
    })
    .catch(error => {
        console.log('Error buscando en la DB: ', error);
    })
    .then(async () => {

        const allUsers = await User.find();
        TeamsArray[0].admin = allUsers[0]._id;
        TeamsArray[1].admin = allUsers[1]._id;
        TeamsArray[2].admin = allUsers[2]._id;
        TeamsArray[3].admin = allUsers[3]._id;
        TeamsArray[4].admin = allUsers[4]._id;
        TeamsArray[5].admin = allUsers[5]._id;
        TeamsArray[6].admin = allUsers[6]._id;
        TeamsArray[7].admin = allUsers[7]._id;
        TeamsArray[8].admin = allUsers[8]._id;
        TeamsArray[9].admin = allUsers[9]._id;

        await Team.insertMany(TeamsArray);
        console.log('Nuevos Teams añadidos a la DB');
    })
    .catch((error) => {
        console.log('Error insertando Teams', error);
    })
    .finally(() => {
        mongoose.disconnect();
    });