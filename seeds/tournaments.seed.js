const mongoose = require('mongoose');
const Tournament = require('../models/Tournament.model');
const User = require('../models/User.model');
const Team = require('../models/Team.model');
const { DB_URL, DB_CONFIG } = require('../db');

// name: { type: String, required: true },
// image: { type: String, required: true },
// teams: [{ type: mongoose.Types.ObjectId, ref: 'Teams' }],
// admin: { type: mongoose.Types.ObjectId, ref: 'Users' },

const TournamentsArray = [
    {
        name: 'Liga de los Mejores',
        image: 'https://previews.123rf.com/images/irinabelokrylova/irinabelokrylova1804/irinabelokrylova180400010/100069336-realista-vector-trofeo-de-f%C3%BAtbol-bal%C3%B3n-de-f%C3%BAtbol-de-la-copa.jpg',
        teams: [],
        admin: '',
    },
    {
        name: 'Liga de la ciudad',
        image: 'https://www.mistrofeos.com/gestion/productos/Copa-tipo-Champions-plateada_1-482.jpg',
        teams: [],
        admin: '',
    },
    {
        name: 'Liga de Upgrade',
        image: 'https://image.freepik.com/foto-gratis/copa-futbol-balon-futbol-sobre-hierba_93675-4360.jpg',
        teams: [],
        admin: '',
    },
];

mongoose.connect(DB_URL, DB_CONFIG)
    .then(async () => {
        console.log('Seed de Torneo ejecutada');
        const allTournaments = await Tournament.find();

        if (allTournaments.length) {
            await Tournament.collection.drop();
            console.log('Colección de Torneo eliminada');
        }
    })
    .catch ((error) => {
        console.log('Error buscando en la DB', error);
    })
    .then(async () => {
        const allTeams = await Team.find();
        TournamentsArray[0].teams = [allTeams[0]._id, allTeams[1]._id, allTeams[2]._id];
        TournamentsArray[1].teams = [allTeams[3]._id, allTeams[4]._id, allTeams[5]._id];
        TournamentsArray[2].teams = [allTeams[6]._id, allTeams[7]._id, allTeams[8]._id];

        const allUsers = await User.find();
        TournamentsArray[0].admin = allUsers[0]._id;
        TournamentsArray[1].admin = allUsers[1]._id;
        TournamentsArray[2].admin = allUsers[2]._id;

        await Tournament.insertMany(TournamentsArray);
        console.log('Añadidos nuevos torneos a la DB');
    })
    .catch((error) => {
        console.log('Error insertando Torneos', error);
    })
    .finally(() => {
        mongoose.disconnect();
    });