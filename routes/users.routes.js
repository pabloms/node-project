const express = require('express');
const controller = require('../controllers/user.controller');
const router = express.Router();

router.get('/', controller.UsersGet);

router.get('/:id', controller.UserByIdGet);

module.exports = router;