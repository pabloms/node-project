const express = require('express');
const router = express.Router();
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');
const middleware = require('../middlewares/auth.middleware');

const {
    allTournamentsGet,
    createTournamentPost,
    addTeamPut,
    TournamentByIdGet,
    deleteTournamentByIdPost,
    createTournamentGet,
    TournamentEditPut,
    deleteTeamPost,
} = require('../controllers/tournaments.controller');

router.get('/', allTournamentsGet);

router.get('/create', createTournamentGet);

router.post('/create', [upload.single('image'), uploadToCloudinary], createTournamentPost);

router.post('/add-team/:id', middleware.isTournamentAdmin, addTeamPut);

router.get('/:id', middleware.yourTeamInThisTournament, TournamentByIdGet);

router.post('/:id', middleware.isTournamentAdmin, deleteTournamentByIdPost);

router.post('/edit/:id', middleware.isTournamentAdmin, [upload.single('image'), uploadToCloudinary], TournamentEditPut);

router.post('/delete-team/:id', middleware.isTournamentAdmin, deleteTeamPost)

module.exports = router;