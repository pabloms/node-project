const express = require('express');
const controller = require('../controllers/auth.controller');
const router = express.Router();
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');

router.get('/register', controller.registerGet);
router.post('/register', [upload.single('image'), uploadToCloudinary], controller.registerPost);

router.get('/login', controller.loginGet);
router.post('/login', controller.loginPost);

router.post('/logout', controller.logoutPost);

module.exports = router;