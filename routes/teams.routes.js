const express = require('express');
const controller = require('../controllers/teams.controller');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');
const router = express.Router();
const middleware = require('../middlewares/auth.middleware');

router.get('/', controller.teamsGet);

router.get('/create', controller.createTeamGet);

router.post('/create', [upload.single('image'), uploadToCloudinary], controller.createTeamPost);

router.post('/edit/:id', middleware.isTeamAdmin, [upload.single('image'), uploadToCloudinary], controller.TeamEditPut);

router.get('/:id', controller.TeamByIdGet);

router.post('/:id', middleware.isTeamAdmin, controller.deleteByIdPost);

router.post('/add-user/:id', controller.addUserPut);

router.post('/delete-user/:id', middleware.isTeamAdmin, controller.deleteUserPost)

module.exports = router;