const Tournament = require('../models/Tournament.model');
const Team = require('../models/Team.model');
const User = require('../models/User.model');

const allTournamentsGet = async (req, res, next) => {
    try {
        const tournaments = await Tournament.find().populate('team');
        return res.render('tournaments/tournaments', { tournaments });
    }
    catch (error) {
        next(error);
    }
};

const createTournamentGet = (req, res, next) => {
    return res.render('tournaments/create');
};

const createTournamentPost = async (req, res, next) => {
    try {
        const { name } = req.body;
        const admin = req.user._id;
        const image = req.image_url ? req.image_url : 'https://images-na.ssl-images-amazon.com/images/I/61wtIabY7mL._AC_SX522_.jpg'

        const newTournament = new Tournament({ name, image, admin });

        const addTournamentToUser = await User.findByIdAndUpdate(
            admin,
            { $addToSet: { tournaments: newTournament._id}},
            { new: true }
        )

        const createdTournament = await newTournament.save();

        return res.render('tournaments/tournament', { tournament: createdTournament });
    }
    catch (error) {
        return next(error);
    }
}

const addTeamPut = async (req, res, next) => {
    try {
        const { id } = req.params;
        const teamId = req.body.teamId;

        const updatedTournament = await Tournament.findByIdAndUpdate(
            id,
            { $addToSet: { teams: teamId }},
            { new: true }
        );

        const updatedTeam = await Team.findByIdAndUpdate(
            teamId,
            { $addToSet: { tournaments: id }},
            { new: true }
        );
        return res.redirect('/tournaments');
    }
    catch (error) {
        next(error);
    }
};

const TournamentByIdGet = async (req, res, next) => {
    const { id } = req.params;

    try {
        const tournament = await Tournament.findById(id).populate('teams');

        return res.render('tournaments/tournament', { tournament });
    }
    catch (error) {
        next(error);
    }
}

const deleteTournamentByIdPost = async (req, res, next) => {
    try {
        const { id } = req.params;
        await Tournament.findByIdAndDelete(id);

        return res.redirect('tournaments/tournaments');
    }
    catch (error) {
        next(error);
    }
}

const TournamentEditPut = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { name } = req.body;
        const image = req.image_url ? req.image_url : 'https://images-na.ssl-images-amazon.com/images/I/61wtIabY7mL._AC_SX522_.jpg'
        const editedTournament = await Tournament.findByIdAndUpdate(
            id,
            { name, image },
            { new: true }
        );
        
        return res.redirect('/tournaments')
    } 
    catch (error) {
        return next(error);
    }
};

const deleteTeamPost = async (req, res, next) => {
    try {
        const { id } = req.params;
        const teamId = req.body.teamId;

        const updatedTournament = await Tournament.findByIdAndUpdate(
            id,
            { $pull: { teams: teamId }},
        );

        const updatedTeam = await Team.findByIdAndUpdate(
            teamId,
            { $pull: { tournaments: id }},
        );
        return res.redirect('/tournaments');
    }
    catch (error) {
        return next(error);
    }
}

module.exports = {
    allTournamentsGet,
    createTournamentPost,
    addTeamPut,
    TournamentByIdGet,
    deleteTournamentByIdPost,
    createTournamentGet,
    TournamentEditPut,
    deleteTeamPost
}