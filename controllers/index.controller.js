const indexGet = (req, res) => {
    const pageTitle = 'Torneos de Futbol';
    return res.render('index', { title: pageTitle, user: req.user });
}

module.exports = { indexGet };