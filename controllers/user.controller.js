const User = require('../models/User.model');

const UserByIdGet = async (req, res, next) => {
    const { id } = req.params;

    try {
        const user = await User.findById(id).populate('teams').populate('tournaments');
        return res.render('users/user', { user });
    }
    catch (error) {
        next(error);
    }
}

const UsersGet = async (req, res, next) => {
    try {
        const users = await User.find();

        return res.render('users/users', {users: users});

    } catch (error) {
        return next(error);
    };
};

module.exports = { UserByIdGet, UsersGet };