const Team = require('../models/Team.model');
const User = require('../models/User.model');

const teamsGet = async (req, res, next) => {
    try {
        const teams = await Team.find();

        return res.render('teams/teams', {teams: teams});

    } catch (error) {
        return next(error);
    };
};

const createTeamGet = (req, res, next) => {
    return res.render('teams/create');
};

const createTeamPost = async (req, res, next) => {
    try {
        const { name } = req.body;
        const admin = req.user._id;
        const image = req.image_url ? req.image_url : 'https://thumbs.dreamstime.com/b/plantilla-vectorial-del-emblema-escudo-b%C3%A1sico-de-f%C3%BAtbol-sello-equipo-patr%C3%B3n-sobre-fondo-blanco-199422305.jpg'

        const newTeam = new Team({ name, image, admin, users: admin });
        
        const addTeamToUser = await User.findByIdAndUpdate(
            admin,
            { $addToSet: { teams: newTeam._id }},
            { new: true }
        )
        const createdTeam = await newTeam.save();

        return res.render('teams/team', { team: createdTeam })
    }
    catch (error) {
        return next(error);
    }
};

const TeamEditPut = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { name } = req.body;
        const image = req.image_url ? req.image_url : 'https://thumbs.dreamstime.com/b/plantilla-vectorial-del-emblema-escudo-b%C3%A1sico-de-f%C3%BAtbol-sello-equipo-patr%C3%B3n-sobre-fondo-blanco-199422305.jpg'
        const editedTeam = await Team.findByIdAndUpdate(
            id,
            { name, image },
            { new: true }
        );
        
        return res.redirect('/teams')
    } 
    catch (error) {
        return next(error);
    }
};

const TeamByIdGet = async (req, res, next) => {

    const { id } = req.params;

    try {
        const team = await Team.findById(id).populate('users').populate('tournaments');
        return res.render('teams/team', { team });
    } 
    catch (error) {
        return next(error);
    }
};

const deleteByIdPost = async (req, res, next) => {
    try {
        const { id } = req.params;
        await Team.findByIdAndDelete(id);

        return res.redirect('/teams');
    } 
    catch (error) {
        next(error);
    }
};

const addUserPut = async (req, res, next) => {
    try {
        const { id } = req.params;
        const userId = req.user._id;

        const updatedUser = await User.findByIdAndUpdate(
            userId,
            { $addToSet: { teams: id }},
            { new: true }
        );

        const updatedTeam = await Team.findByIdAndUpdate(
            id,
            { $addToSet: { users: userId }},
            { new: true }
        );

        return res.redirect('/teams');
    }
    catch (error) {
        next(error);
    }
};

const deleteUserPost = async (req, res, next) => {
    try {
        const { id } = req.params;
        const userId = req.body.userId;

        const updatedTeam = await Team.findByIdAndUpdate(
            id,
            { $pull: { users: userId }},
        );

        const updatedUser = await User.findByIdAndUpdate(
            userId,
            { $pull: { teams: id }},
        );
        return res.redirect('/teams');
    }
    catch (error) {
        return next(error);
    }
}

module.exports = {
    teamsGet,
    createTeamGet,
    createTeamPost,
    TeamEditPut,
    TeamByIdGet,
    deleteByIdPost,
    addUserPut,
    deleteUserPost
}
