const passport = require('passport');

module.exports = {
    registerGet: (req, res, next) => {
        return res.render('auth/register');
    },

    registerPost: (req, res, next) => {
        passport.authenticate('register', (error, user) => {
            if (error) {
                return next(error);
            }

            req.logIn(user, (error) => {
                if (error) {
                    return next(error);
                }

                return res.redirect('/');
            });
        })
        (req)
    },

    loginGet: (req, res, next) => {
        return res.render('auth/login');
    },

    loginPost: (req, res, next) => {
        passport.authenticate('login', (error, user) => {
            if (error) {
                return next(error);
            }

            req.logIn(user, (error) => {
                if (error) {
                    return next(error);
                }

                res.redirect('/');
            })
        })(req)
    },

    logoutPost: (req, res, next) => {
        if(req.user) {
            req.logout();

            req.session.destroy(() => {
                res.clearCookie('connect.sid');

                return res.redirect('/');
            })
        }
        else {
            return res.status.json('No user logged before');
        }
    }
}