const mongoose = require('mongoose');
const DB_URL = process.env.DB_URL || 'mongodb://localhost:27017/node-proyect';
const DB_CONFIG = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

const connect = () => {
    mongoose.connect(DB_URL, DB_CONFIG)
        .then((res) => {
            const { name, host } = res.connection;
            console.log(`Connected succesfully to ${name} - ${host}`);
        })
}

module.exports = { DB_URL, DB_CONFIG, connect };