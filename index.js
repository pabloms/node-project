const express = require('express');
const server = express();

const db = require('./db');
db.connect();
require('./authentication');

const session = require('express-session');
const MongoStore = require('connect-mongo');

const dotenv = require('dotenv');
dotenv.config();
const PORT = process.env.PORT || 5000;

server.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 15 * 24 * 60 * 60 * 1000,
        },
        store: MongoStore.create({ mongoUrl: db.DB_URL }),
    })
);

const passport = require('passport');
server.use(passport.initialize());
server.use(passport.session());

const path = require('path');
const hbs = require('hbs');

server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');

server.use(express.static(path.join(__dirname, 'public')));

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

// Rutas requeridas
const indexRoutes = require('./routes/index.routes');
server.use('/', indexRoutes);
const authRoutes = require('./routes/auth.routes');
server.use('/auth', authRoutes);
const tournamentsRoutes = require('./routes/tournaments.routes');
server.use('/tournaments', tournamentsRoutes);
const userRoutes = require('./routes/users.routes');
server.use('/users', userRoutes);
const teamsRoutes = require('./routes/teams.routes');
server.use('/teams', teamsRoutes);


// Error Handler
server.use("*", (req, res, next) => {
    const error = new Error ("Route not found");
    error.status = 404;
    return next(error);
});

server.use((error, req, res, next) => {
    const errorStatus = error.status || 500;
    const errorMessage = error.message || "Unexpected Error";
    return res.render('error', { status: errorStatus, message: errorMessage });
})

server.listen(PORT, () => {
    console.log(`Server is running in http://localhost:${PORT}`);
});