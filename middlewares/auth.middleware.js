const Team = require('../models/Team.model');
const Tournament = require('../models/Tournament.model');
const User = require('../models/User.model');

const isTeamAdmin = async (req, res, next) => {
    const id = req.params.id;
    const team = await Team.findById(id);

    if (team.admin.equals(req.user._id)) {
        return next();
    } else {
        const error = new Error("You are not the admin of this Team");
        error.status = 403;
        return next(error);
    }
}

const isTournamentAdmin = async (req, res, next) => {
    const id = req.params.id;
    const tournament = await Tournament.findById(id);

    if (tournament.admin.equals(req.user._id)) {
        return next();
    } else {
        const error = new Error("You are not the admin of this Tournament");
        error.status = 403;
        return next(error);
    }
}

const yourTeamInThisTournament = async (req, res, next) => {
    const id = req.params.id;
    const tournament = await Tournament.findById(id);

    if ((tournament.admin.equals(req.user.teams[0])) || (tournament.admin.equals(req.user.teams[1])) || (tournament.admin.equals(req.user.teams[2])) || (tournament.admin.equals(req.user._id))) {
        return next();
    } else {
        const error = new Error("Your team is not part of this Tournament");
        error.status = 403;
        return next(error);
    }
}

module.exports = { isTeamAdmin, isTournamentAdmin, yourTeamInThisTournament };