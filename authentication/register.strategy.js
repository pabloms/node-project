const localStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User.model');

const saltRounds = 10;

// validación
const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePass = (password) => {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return re.test(String(password));
}


const registerStrategy = new localStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        try {
            const existingUser = await User.findOne({ email: email });

            if (existingUser) {
                const error = new Error('This email is already registered');
                return done(error);
            }

            const isValidEmail = validateEmail(email);
            if (!isValidEmail) {
                const error = new Error ('Invalid email, please check again.');
                return done(error);
            }

            const isValidPassword = validatePass(password);
            if (!isValidPassword) {
                const error = new Error ('The password has to be between 6 and 20 characters, it must have one Capital Letter and a number');
                return done(error);
            }

            // Si llega hasta aquí, registro al usuario
            const hash = await bcrypt.hash(password, saltRounds);

            const newUser = new User ({
                email: email,
                password: hash,
                username: req.body.username,
                personalInfo: {
                    age: req.body.age,
                    nationality: req.body.nationality,
                    image: req.image_url ? req.image_url : 'https://st4.depositphotos.com/29453910/37778/v/600/depositphotos_377783970-stock-illustration-hand-drawn-modern-man-avatar.jpg',
                    position: req.body.position,
                },
                stats: {
                    goals: 0,
                    asists: 0,
                },
                teams: [],
            });

            const savedUser = await newUser.save();

            return done(null, savedUser);
        }
        catch (error) {
            return done(error);
        }
    }
)

module.exports = registerStrategy;