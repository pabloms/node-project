const localStrategy = require('passport-local').Strategy;
const User = require('../models/User.model');
const bcrypt = require('bcrypt');

const loginStrategy = new localStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        try {
            const exitingUser = await User.findOne( {email} );

            if(!exitingUser) {
                const error = new Error("User doesn't exists");
                error.status = 401;
                return done(error);
            }

            const isValidPassword = await bcrypt.compare(password, exitingUser.password);

            if(!isValidPassword) {
                const error = new Error('Password is invalid');
                return done(error);
            }

            return done(null, exitingUser);
        }
        catch (error) {
            return done(error);
        }
    }
)

module.exports = loginStrategy;