const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const teamSchema = new Schema (
    {
        name: { type: String, required: true },
        image: { type: String, required: true },
        tournaments: [{ type: mongoose.Types.ObjectId, ref: 'Tournaments' }],
        users: [{ type: mongoose.Types.ObjectId, ref: 'Users' }],
        admin: {type: mongoose.Types.ObjectId, ref: 'Users'},
    },
    {
        timestamp: true,
    }
);

const Team = mongoose.model('Teams', teamSchema);
module.exports = Team;