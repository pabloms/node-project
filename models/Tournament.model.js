const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tournamentSchema = new Schema (
    {
        name: { type: String, required: true },
        image: { type: String, required: true },
        teams: [{ type: mongoose.Types.ObjectId, ref: 'Teams' }],
        admin: { type: mongoose.Types.ObjectId, ref: 'Users' },
    },
    {
        timestamps: true,
    }
);

const Tournament = mongoose.model('Tournaments', tournamentSchema);
module.exports = Tournament;