const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema (
    {
        email: { type: String, required: true },
        password: { type: String, required: true },
        username: { type: String, required: true },
        personalInfo: {
            age: { type: Number },
            nationality: { type: String },
            image: { type: String },
            position: { type: String, required: true},
        },
        stats: {
            goals: { type: Number },
            asists: { type: Number},
        },
        teams: [{ type: mongoose.Types.ObjectId, ref:'Teams' }],
        tournaments: [{ type: mongoose.Types.ObjectId, ref:'Tournaments'}]
    },
    {
        timestamps: true,
    }
);

const User = mongoose.model('Users', userSchema);
module.exports = User;